const express = require('express');
const router = express.Router();

const authMiddleware = require('../middlewares/authMiddleware');

const {
  getUserNotes,
  addNote,
  getNoteById,
  updateNote,
  deleteNote,
  changeNoteState
} = require('../controllers/noteController');

router.get('/notes', authMiddleware, getUserNotes);
router.post('/notes', authMiddleware, addNote);

router.get('/notes/:id', authMiddleware, getNoteById);
router.patch('/notes/:id', authMiddleware, changeNoteState);
router.put('/notes/:id', authMiddleware, updateNote);
router.delete('/notes/:id', authMiddleware, deleteNote);

module.exports = router;
