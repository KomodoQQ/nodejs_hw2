const mongoose = require('mongoose');

const noteSchema = new mongoose.Schema({
  userId: {
    required: true,
    type: mongoose.Types.ObjectId
  },
  text: {
    required: true,
    type: String
  },
  completed: {
    type: Boolean,
    default: false
  },
  createdDate: {
    type: Date,
    default: Date.now
  }
}, {versionKey: false});

module.exports = mongoose.model('note', noteSchema);