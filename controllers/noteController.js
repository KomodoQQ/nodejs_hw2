const Note = require('../models/note');

module.exports.getUserNotes = (request, response) => {
  Note.find({ userId: request.user._id })
    .then((notes) => {
      if (!notes) {
        return response.status(400).json({ message: 'Notes not found' });
      }

      response.json({ notes });
    })
    .catch((err) => {
      response.status(500).json({ message: err.message });
    });
};

module.exports.addNote = (request, response) => {
  const { text } = request.body;
  if (!text) {
    return response.status(400).json({ message: 'Empty note found in request body. Add some text' });
  }

  const note = new Note({ userId: request.user._id, text });
  note
    .save()
    .then(() => {
      response.json({ message: 'Success' });
    })
    .catch((err) => {
      response.status(500).json({ message: err.message });
    });
};

module.exports.getNoteById = (request, response) => {
  Note.findById(request.params.id)
    .exec()
    .then((note) => {
      if (!note) {
        return response.status(400).json({ message: 'Note not found' });
      }

      if (note.userId.toString() !== request.user._id) {
        return response.status(400).json({ message: "You don't have access to this note" });
      }

      response.json({ note });
    })
    .catch((err) => {
      response.status(500).json({ message: err.message });
    });
};

module.exports.updateNote = (request, response) => {
  Note.findByIdAndUpdate(request.params.id, { text: request.body.text })
    .exec()
    .then((note) => {
      if (!note) {
        return response.status(400).json({ message: 'Note not found' });
      }

      if (note.userId.toString() !== request.user._id) {
        return response.status(400).json({ message: "You don't have access to this note" });
      }

      response.json({ message: 'Success' });
    })
    .catch((err) => {
      response.status(500).json({ message: err.message });
    });
};

module.exports.changeNoteState = (request, response) => {
  Note.findById(req.params.id)
    .exec()
    .then((note) => {
      if (!note) {
        return response.status(400).json({ message: 'Note not found' });
      }

      if (note.userId.toString() !== request.user._id) {
        return response.status(400).json({ message: "You don't have access to this note" });
      }

      note.completed = !note.completed;
      return note.save();
    })
    .then(() => {
      response.json({ message: 'Success' });
    })
    .catch((err) => {
      response.status(500).json({ message: err.message });
    });
};

module.exports.deleteNote = (request, response) => {
  Note.findByIdAndDelete(request.params.id)
    .exec()
    .then((note) => {
      if (!note) {
        return response.status(400).json({ message: 'Note not found' });
      }

      if (note.userId.toString() !== request.user._id) {
        return response.status(400).json({ message: "You don't have access to this note" });
      }

      response.json({ message: 'Success' });
    })
    .catch((err) => {
      response.status(500).json({ message: err.message });
    });
};
